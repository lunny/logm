package main

import (
	"math"
	"os"

	"github.com/blevesearch/bleve"
	"github.com/blevesearch/bleve/analysis/analyzer/simple"
	"github.com/blevesearch/bleve/search/query"
	logger "github.com/lunny/log"
	gouuid "github.com/satori/go.uuid"
)

// BleveStore an implementation via bleve index
type BleveStore struct {
	location   string
	indexer    bleve.Index
	indexQueue chan *Log
}

var (
	_                      Store = &BleveStore{}
	defaultIndexQueueSize        = 50
	DefaultSearchMaxNumber       = 50 // 默认一次最多显示结果
)

// NewBleveStore create new bleve store
func NewBleveStore(location string) (*BleveStore, error) {
	store := &BleveStore{
		location:   location,
		indexQueue: make(chan *Log, defaultIndexQueueSize),
	}

	var err error
	store.indexer, err = loadIndexer(location)
	if err != nil {
		return nil, err
	}

	go store.processIndexQueue()

	return store, nil
}

func loadIndexer(location string) (bleve.Index, error) {
	_, err := os.Stat(location)
	if err != nil {
		if os.IsNotExist(err) {
			docMapping := bleve.NewDocumentMapping()
			docMapping.AddFieldMappingsAt("Time", bleve.NewNumericFieldMapping())

			textFieldMapping := bleve.NewTextFieldMapping()
			textFieldMapping.Analyzer = simple.Name
			docMapping.AddFieldMappingsAt("ID", textFieldMapping)
			docMapping.AddFieldMappingsAt("Source", textFieldMapping)
			docMapping.AddFieldMappingsAt("IP", textFieldMapping)
			docMapping.AddFieldMappingsAt("Level", textFieldMapping)
			docMapping.AddFieldMappingsAt("Content", textFieldMapping)

			mapping := bleve.NewIndexMapping()
			mapping.AddDocumentMapping("logs", docMapping)

			return bleve.New(location, mapping)
		}
		return nil, err
	}

	return bleve.Open(location)
}

func (b *BleveStore) processIndexQueue() {
	for {
		select {
		case log := <-b.indexQueue:
			logger.Debug("received one log", *log)
			if err := b.indexer.Index(log.ID, log); err != nil {
				logger.Errorf("indexer.Index: %v\n", err)
			}
		}
	}
}

// Save saves log to indexer
func (b *BleveStore) Save(log *Log) error {
	log.ID = gouuid.NewV4().String()
	go func() {
		b.indexQueue <- log
	}()
	return nil
}

func numericQuery(value int64, field string) *query.NumericRangeQuery {
	f := float64(value)
	tru := true
	q := bleve.NewNumericRangeInclusiveQuery(&f, &f, &tru, &tru)
	q.SetField(field)
	return q
}

func numericRangeQuery(start, end int64, field string) *query.NumericRangeQuery {
	fStart := float64(start)
	fEnd := float64(end)
	tru := true
	q := bleve.NewNumericRangeInclusiveQuery(&fStart, &fEnd, &tru, &tru)
	q.SetField(field)
	return q
}

// Search search logs according conditions
func (b *BleveStore) Search(opt SearchOpts) ([]*Log, error) {
	if opt.MaxNumbers == 0 {
		opt.MaxNumbers = DefaultSearchMaxNumber
	}

	var querys []query.Query
	if len(opt.Level) > 0 {
		querys = append(querys, bleve.NewPhraseQuery([]string{opt.Level}, "Level"))
	}
	if len(opt.Source) > 0 {
		querys = append(querys, bleve.NewPhraseQuery([]string{opt.Source}, "Source"))
	}
	if len(opt.IP) > 0 {
		querys = append(querys, bleve.NewPhraseQuery([]string{opt.IP}, "IP"))
	}

	isStartZero := opt.StartTime.IsZero()
	isEndZero := opt.EndTime.IsZero()
	if !isStartZero && !isEndZero {
		var start, end int64 = 0, math.MaxInt64
		if !isStartZero {
			start = opt.StartTime.Unix()
		}
		if !isEndZero {
			end = opt.EndTime.Unix()
		}
		querys = append(querys, numericRangeQuery(start, end, "Time"))
	}

	if len(querys) <= 0 {
		querys = append(querys, numericRangeQuery(0, math.MaxInt64, "Time"))
	}

	indexerQuery := bleve.NewConjunctionQuery(querys...)
	search := bleve.NewSearchRequestOptions(indexerQuery, opt.MaxNumbers, opt.Skip, false)
	search.SortBy([]string{"-Time"})
	search.Fields = []string{"Source", "Level", "Time", "Content", "IP"}
	result, err := b.indexer.Search(search)
	if err != nil {
		return nil, err
	}

	logs := make([]*Log, len(result.Hits))
	for i, hit := range result.Hits {
		logs[i] = &Log{
			Source:  hit.Fields["Source"].(string),
			Level:   hit.Fields["Level"].(string),
			Time:    int64(hit.Fields["Time"].(float64)),
			Content: hit.Fields["Content"].(string),
			IP:      hit.Fields["IP"].(string),
		}
	}
	return logs, nil
}

// Close closes the indexer
func (b *BleveStore) Close() error {
	if b.indexer != nil {
		b.indexer.Close()
	}
	return nil
}
