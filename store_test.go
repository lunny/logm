package main

import (
	"os"
	"reflect"
	"testing"
	"time"
)

// TestSaveLog test save log
func TestBleveLog(t *testing.T) {
	store, err := NewBleveStore("./index")
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		store.Close()
		os.Remove("./index")
	}()

	log := &Log{
		Source:  "webserver1",
		Time:    time.Now().Unix(),
		Level:   "Info",
		Content: "web server started",
	}
	err = store.Save(log)
	if err != nil {
		t.Fatal(err)
	}

	// wait for log saved
	time.Sleep(time.Second * 5)

	logs, err := store.Search(SearchOpts{
		Source: "webserver1",
	})
	if err != nil {
		t.Fatal(err)
	}

	if len(logs) != 1 {
		t.Fatal("there should be 1 log")
	}

	log.ID = ""
	if !reflect.DeepEqual(logs[0], log) {
		t.Fatal("store content should be the same with origil log", *logs[0], *log)
	}
}
