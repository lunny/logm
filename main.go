package main

import (
	"flag"
	"fmt"
	"html/template"
	"path/filepath"

	"github.com/lunny/config"
	"github.com/lunny/log"
	"github.com/lunny/tango"
	"github.com/tango-contrib/basicauth"
	"github.com/tango-contrib/renders"
)

var (
	store   Store
	cfgpath = flag.String("cfg", "./cfg.ini", "config file path")
)

// Home homepage
type Home struct {
	tango.Ctx
	renders.Renderer
}

func (h *Home) Get() error {
	limit := h.FormInt("limit")
	if limit == 0 {
		limit = DefaultSearchMaxNumber
	}
	page := h.FormInt("page")
	if page < 0 {
		page = 0
	}
	level := h.Form("level")
	var opt = SearchOpts{
		Level:      level,
		Source:     h.Form("source"),
		MaxNumbers: limit,
		Skip:       page * limit,
	}
	logs, err := store.Search(opt)
	if err != nil {
		return err
	}
	if len(logs) <= 0 {
		page = page - 1
	}
	if page < 0 {
		page = 0
	}
	return h.Render("home.html", renders.T{
		"source": h.Form("source"),
		"logs":   logs,
		"page":   page,
		"level":  level,
	})
}

func main() {
	flag.Parse()

	cfg, err := config.Load(*cfgpath)
	if err != nil {
		fmt.Println(err)
		return
	}

	indexPath := filepath.Join(cfg.Get("data_dir"), "index")

	store, err = NewBleveStore(indexPath)
	if err != nil {
		panic(err)
	}

	defer store.Close()

	t := tango.Classic()
	t.Use(renders.New(renders.Options{
		Directory: "templates",
		Reload:    true,
		Funcs: template.FuncMap{
			"Sub": renders.Sub,
			"Add": renders.Add,
		},
	}))
	t.Post("/input/json", func(ctx *tango.Context) {
		var l Log
		err := ctx.DecodeJson(&l)
		if err != nil {
			log.Error(err)
			return
		}
		err = store.Save(&l)
		if err != nil {
			log.Error(err)
			return
		}
	})
	t.Get("/", new(Home), basicauth.New("admin", "135a246z"))
	t.Run(cfg.Get("listen"))
}
