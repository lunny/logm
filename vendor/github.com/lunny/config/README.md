config
======

Config is a simple config package for parse and save config files.

The config format like below:

```
a=b
c=d
```