package main

import (
	"strings"
	"time"
)

type SearchOpts struct {
	StartTime  time.Time
	EndTime    time.Time
	Level      string
	Source     string
	IP         string
	Skip       int
	MaxNumbers int
}

type Log struct {
	ID      string
	Source  string
	Time    int64
	Level   string
	Content string
	IP      string
}

func (l *Log) FormatTime(format string) string {
	return time.Unix(l.Time, 0).Format(format)
}

func (l *Log) Color() string {
	if strings.EqualFold(l.Level, "INFO") {
		return "info"
	} else if strings.EqualFold(l.Level, "WARN") {
		return "warning"
	} else if strings.EqualFold(l.Level, "ERROR") {
		return "danger"
	}
	return ""
}

type Store interface {
	Save(*Log) error
	Search(SearchOpts) ([]*Log, error)
	Close() error
}
